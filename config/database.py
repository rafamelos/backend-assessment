from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import os
from dotenv import load_dotenv
from pathlib import Path  # python3 only
env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)


DATABASE_URI = 'postgres+psycopg2://' + os.getenv("DB_USER") + ':' + os.getenv("DB_PASSWORD") + '@' \
               + os.getenv("DB_HOST") + ':' + os.getenv("DB_PORT") + '/' + os.getenv("DB_NAME")
db_schema = os.getenv("DB_SCHEMA")

engine = create_engine(DATABASE_URI, connect_args={'options': '-csearch_path={}'.format(db_schema)})
test_engine = create_engine(DATABASE_URI, connect_args={'options': '-csearch_path={}'.format(db_schema+"_test")})

Session = sessionmaker(bind=engine)

Base = declarative_base()
