from sqlalchemy import Column, Integer, ForeignKey, DateTime
from sqlalchemy.orm import relationship
from pydantic import BaseModel
from datetime import datetime
from config.database import Base


class OrderOrm(Base):
    __tablename__ = 'client_order'
    id = Column(Integer, primary_key=True, index=True)
    date = Column(DateTime, nullable=False)
    attendance_id = Column(Integer, ForeignKey('attendance.id'))
    attendance = relationship("AttendanceOrm", back_populates="orders")
    client_id = Column(Integer, ForeignKey('client.id'))
    client = relationship("ClientOrm", back_populates="orders")
    products = relationship("OrderProductOrm", back_populates="order")


class Order(BaseModel):
    id: int
    date: datetime

    class Config:
        orm_mode = True
