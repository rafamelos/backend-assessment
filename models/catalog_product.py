from sqlalchemy import Column, Integer, Float, ForeignKey
from sqlalchemy.orm import relationship
from config.database import Base


class CatalogProductOrm(Base):
    __tablename__ = 'catalog_product'
    catalog_id = Column(Integer, ForeignKey('catalog.id'), primary_key=True)
    product_id = Column(Integer, ForeignKey('product.id'), primary_key=True)
    price = Column(Float, nullable=False, default=0)
    catalog = relationship("CatalogOrm", back_populates="products")
    product = relationship("ProductOrm", back_populates="catalogs")
