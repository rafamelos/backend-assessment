from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from pydantic import BaseModel, constr
from config.database import Base
from models.user_group import UserGroup


class UserOrm(Base):
    __tablename__ = 'platform_user'
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255), nullable=False)
    email = Column(String(255), nullable=False)
    password = Column(String(512), nullable=False)
    group_id = Column(Integer, ForeignKey('user_group.id'))
    group = relationship("UserGroupOrm", back_populates="users")
    attendances = relationship("AttendanceOrm", back_populates="user")


class UserBase(BaseModel):
    email: constr(max_length=255)


class User(UserBase):
    id: int
    name: constr(max_length=255)
    group: UserGroup

    class Config:
        orm_mode = True


class UserSecurity(UserBase):
    password: constr(max_length=512)

    class Config:
        orm_mode = True


class UserCreate(UserBase):
    name: constr(max_length=255)
    password: constr(max_length=20)
    group_id: int
