from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import relationship
from pydantic import BaseModel, constr
from datetime import datetime
from config.database import Base


class ClientOrm(Base):
    __tablename__ = 'client'
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255), nullable=False)
    phone = Column(String(30), nullable=False)
    created_at = Column(DateTime)
    orders = relationship("OrderOrm", back_populates="client")
    stocks = relationship("StockOrm", back_populates="client")
    catalogs = relationship("CatalogOrm", back_populates="client")


class ClientBase(BaseModel):
    name: constr(max_length=255)
    phone: constr(max_length=30)
    created_at: datetime = None


class Client(ClientBase):
    id: int

    class Config:
        orm_mode = True


class ClientCreate(ClientBase):
    pass
