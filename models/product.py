from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, Float, DateTime
from sqlalchemy.orm import relationship
from pydantic import BaseModel, constr
from datetime import datetime
from config.database import Base


class ProductOrm(Base):
    __tablename__ = 'product'
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255), nullable=False)
    type = Column(String(255), nullable=False)
    price = Column(Float, nullable=False)
    created_at = Column(DateTime, default=datetime.utcnow())
    orders = relationship("OrderProductOrm", back_populates="product")
    stocks = relationship("StockProductOrm", back_populates="product")
    catalogs = relationship("CatalogProductOrm", back_populates="product")


class ProductBase(BaseModel):
    name: constr(max_length=255)
    type: constr(max_length=255)
    price: float
    created_at: datetime = None


class Product(ProductBase):
    id: int

    class Config:
        orm_mode = True


class ProductCreate(ProductBase):
    pass


class ProductForStock(ProductBase):
    id: int = None
