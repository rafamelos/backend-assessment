from sqlalchemy import Column, Integer, ForeignKey, DateTime
from sqlalchemy.orm import relationship
from pydantic import BaseModel
from config.database import Base
from datetime import datetime


class CatalogOrm(Base):
    __tablename__ = 'catalog'
    id = Column(Integer, primary_key=True, index=True)
    date = Column(DateTime, nullable=False)
    client_id = Column(Integer, ForeignKey('client.id'))
    client = relationship("ClientOrm", back_populates="catalogs")
    products = relationship("CatalogProductOrm", back_populates="catalog")


class CatalogBase(BaseModel):
    date: datetime
    client_id: int


class CatalogCreate(CatalogBase):
    pass


class Catalog(CatalogBase):
    id: int

    class Config:
        orm_mode = True
