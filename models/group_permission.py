from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from pydantic import BaseModel
from config.database import Base
from models.permission import Permission


class GroupPermissionOrm(Base):
    __tablename__ = 'group_permission'
    group_id = Column(Integer, ForeignKey('user_group.id'), primary_key=True)
    permission_id = Column(Integer, ForeignKey('permission.id'), primary_key=True)
    read = Column(Boolean)
    write = Column(Boolean)
    group = relationship("UserGroupOrm", back_populates="permissions")
    permission = relationship("PermissionOrm", back_populates="groups")


class GroupPermissionBase(BaseModel):
    read: bool
    write: bool


class GroupPermissionForGroup(GroupPermissionBase):
    permission: Permission

    class Config:
        orm_mode = True
