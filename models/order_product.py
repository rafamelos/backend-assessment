from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from config.database import Base


class OrderProductOrm(Base):
    __tablename__ = 'order_product'
    order_id = Column(Integer, ForeignKey('client_order.id'), primary_key=True)
    product_id = Column(Integer, ForeignKey('product.id'), primary_key=True)
    quantity = Column(Integer, nullable=False, default=0)
    order = relationship("OrderOrm", back_populates="products")
    product = relationship("ProductOrm", back_populates="orders")
