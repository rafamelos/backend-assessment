from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship
from config.database import Base
from pydantic import BaseModel
from models.product import ProductForStock, Product


class StockProductOrm(Base):
    __tablename__ = 'stock_product'
    stock_id = Column(Integer, ForeignKey('stock.id'), primary_key=True)
    product_id = Column(Integer, ForeignKey('product.id'), primary_key=True)
    quantity = Column(Integer, nullable=False, default=0)
    stock = relationship("StockOrm", back_populates="products")
    product = relationship("ProductOrm", back_populates="stocks")


class StockProductBase(BaseModel):
    quantity: int


class StockProductCreate(StockProductBase):
    product_id: int = None
    product: ProductForStock = None


class StockProductForStock(StockProductBase):
    product: Product

    class Config:
        orm_mode = True
