from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from pydantic import BaseModel, constr
from config.database import Base
from typing import List
from models.group_permission import GroupPermissionForGroup


class UserGroupOrm(Base):
    __tablename__ = 'user_group'
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255), nullable=False)
    type = Column(String(255), nullable=False)
    permissions = relationship("GroupPermissionOrm", back_populates="group")
    users = relationship("UserOrm", back_populates="group")


class UserGroup(BaseModel):
    id: int
    name: constr(max_length=255)
    type: constr(max_length=255)
    permissions: List[GroupPermissionForGroup]

    class Config:
        orm_mode = True
