from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.orm import relationship
from pydantic import BaseModel, constr
from config.database import Base


class PermissionOrm(Base):
    __tablename__ = 'permission'
    id = Column(Integer, primary_key=True, index=True)
    code = Column(String(255), nullable=False, unique=True)
    name = Column(String(255), nullable=False)
    groups = relationship("GroupPermissionOrm", back_populates="permission")


class Permission(BaseModel):
    id: int
    code: constr(max_length=255)
    name: constr(max_length=255)

    class Config:
        orm_mode = True
