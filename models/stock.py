from sqlalchemy import Column, Integer, ForeignKey, DateTime
from sqlalchemy.orm import relationship
from pydantic import BaseModel, constr
from datetime import datetime
from config.database import Base
from typing import List
from models.stock_product import StockProductCreate, StockProductForStock
from models.client import Client


class StockOrm(Base):
    __tablename__ = 'stock'
    id = Column(Integer, primary_key=True, index=True)
    date = Column(DateTime, nullable=False, default=datetime.utcnow())
    client_id = Column(Integer, ForeignKey('client.id'))
    client = relationship("ClientOrm", back_populates="stocks")
    products = relationship("StockProductOrm", back_populates="stock")


class StockBase(BaseModel):
    date: datetime
    client_id: int


class StockCreate(StockBase):
    products: List[StockProductCreate]


class Stock(StockBase):
    id: int
    client: Client
    products: List[StockProductForStock]

    class Config:
        orm_mode = True
