from sqlalchemy import Column, Integer, ForeignKey, DateTime
from sqlalchemy.orm import relationship
from config.database import Base
from pydantic import BaseModel
from datetime import datetime


class AttendanceOrm(Base):
    __tablename__ = 'attendance'
    id = Column(Integer, primary_key=True, index=True)
    date = Column(DateTime, nullable=False)
    user_id = Column(Integer, ForeignKey('platform_user.id'))
    user = relationship("UserOrm", back_populates="attendances")
    orders = relationship("OrderOrm", back_populates="attendance")


class AttendanceBase(BaseModel):
    date: datetime
    user_id: int


class AttendanceCreate(AttendanceBase):
    pass


class Attendance(AttendanceBase):
    id: int

    class Config:
        orm_mode = True
