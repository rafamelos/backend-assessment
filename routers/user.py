from fastapi import APIRouter, Depends
from models.user import User, UserCreate
from sqlalchemy.orm import Session
from utils.database import get_db
from crud import user as crud
from security.authentication import HasAccess

router = APIRouter()


@router.post("/new-user", response_model=User, dependencies=[Depends(HasAccess("user", True, False))])
async def create_user(new_user: UserCreate, db: Session = Depends(get_db)):
    return crud.save(db, new_user)
