from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from models.stock import Stock, StockCreate
from utils.database import get_db
from crud import stock as crud
from security.authentication import HasAccess

router = APIRouter()


@router.post("/", response_model=Stock, dependencies=[Depends(HasAccess("stock", True, False))])
async def create_stock(stock: StockCreate, db: Session = Depends(get_db)):
    return crud.save(db, stock)
