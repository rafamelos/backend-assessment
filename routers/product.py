from fastapi import APIRouter, Depends, HTTPException
from models.product import Product, ProductCreate
from typing import List
from sqlalchemy.orm import Session
from crud import product as crud
from security.authentication import HasAccess
from utils.database import get_db


router = APIRouter()


@router.get("/", response_model=List[Product], dependencies=[Depends(HasAccess("product", False, True))])
async def get_products(db: Session = Depends(get_db)):
    return crud.find_all(db)


@router.get("/{id}", response_model=Product, dependencies=[Depends(HasAccess("product", False, True))])
async def get_product(id: int, db: Session = Depends(get_db)):
    product: Product = crud.find_by_id(db, id)
    if product is None:
        raise HTTPException(status_code=404, detail="Product not found")
    return product


@router.post("/", response_model=Product, dependencies=[Depends(HasAccess("product", True, False))])
async def create_product(product: ProductCreate, db: Session = Depends(get_db)):
    return crud.save(db, product)


@router.put("/", response_model=Product, dependencies=[Depends(HasAccess("product", True, False))])
async def update_product(product: Product, db: Session = Depends(get_db)):
    return crud.update(db, product)


@router.delete("/{id}", response_model=Product, dependencies=[Depends(HasAccess("product", True, False))])
async def remove_product(id: int, db: Session = Depends(get_db)):
    product: Product = crud.delete(db, id)
    if product is None:
        raise HTTPException(status_code=404, detail="Product not found to be deleted")
    return product
