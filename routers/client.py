from fastapi import APIRouter, Depends, HTTPException
from models.client import Client, ClientCreate
from typing import List
from sqlalchemy.orm import Session
from utils.database import get_db
from crud import client as crud
from security.authentication import HasAccess


router = APIRouter()


@router.get("/", response_model=List[Client], dependencies=[Depends(HasAccess("client", False, True))])
async def get_clients(db: Session = Depends(get_db)):
    return crud.find_all(db)


@router.get("/{id}", response_model=Client, dependencies=[Depends(HasAccess("client", False, True))])
async def get_client(id: int, db: Session = Depends(get_db)):
    client: Client = crud.find_by_id(db, id)
    if client is None:
        raise HTTPException(status_code=404, detail="Client not found")
    return client


@router.post("/", response_model=Client, dependencies=[Depends(HasAccess("client", True, False))])
async def create_client(client: ClientCreate, db: Session = Depends(get_db)):
    return crud.save(db, client)


@router.put("/", response_model=Client, dependencies=[Depends(HasAccess("client", True, False))])
async def update_client(client: Client, db: Session = Depends(get_db)):
    return crud.update(db, client)


@router.delete("/{id}", response_model=Client, dependencies=[Depends(HasAccess("client", True, False))])
async def remove_client(id: int, db: Session = Depends(get_db)):
    client: Client = crud.delete(db, id)
    if client is None:
        raise HTTPException(status_code=404, detail="Client not found to be deleted")
    return client
