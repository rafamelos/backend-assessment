from fastapi import Depends, HTTPException
from starlette.status import HTTP_401_UNAUTHORIZED, HTTP_403_FORBIDDEN
from sqlalchemy.orm import Session
from crud.user import find_user_security_by_email, find_by_email
import config.security as config
from security.models import Token, TokenData
from security import crypt
from datetime import datetime, timedelta
from utils.database import get_db
from models.user import User
import jwt


def authenticate_user(db: Session, email: str, password: str):
    user = find_user_security_by_email(db, email)
    if not user:
        return False
    if not crypt.verify_password(password, user.password):
        return False
    return user


def create_access_token(data: dict) -> Token:
    expire = datetime.utcnow() + timedelta(days=config.ACCESS_TOKEN_EXPIRE_DAYS)
    to_encode = data.copy()
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, config.SECRET_KEY, algorithm=config.ALGORITHM)
    return Token(access_token=encoded_jwt, token_type="bearer")


def is_authenticated(db: Session = Depends(get_db), token: str = Depends(config.oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, config.SECRET_KEY, algorithms=[config.ALGORITHM])
        email: str = payload.get("sub")
        if email is None:
            raise credentials_exception
        token_data = TokenData(email=email)
    except jwt.PyJWTError:
        raise credentials_exception
    user = find_by_email(db, email=token_data.email)
    if user is None:
        raise credentials_exception
    return user


class HasAccess:
    def __init__(self, code: str = None, write: bool = False, read: bool = False):
        self.code = code
        self.write = write
        self.read = read

    def __call__(self, user: User = Depends(is_authenticated)):
        forbidden_exception = HTTPException(
            status_code=HTTP_403_FORBIDDEN,
            detail="You dont have permission to do this."
        )
        if self.code is None:
            return True
        for group_permission in user.group.permissions:
            if group_permission.permission.code == self.code:
                if (group_permission.read or not self.read) and (group_permission.write or not self.write):
                    return True
        raise forbidden_exception
