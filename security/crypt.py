from config.security import pwd_context


def verify_password(password: str, saved_password: str):
    return pwd_context.verify(password, saved_password)


def create_password_hash(password: str):
    return pwd_context.hash(password)