from fastapi import FastAPI, Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from routers import product, client, user, stock
from starlette.requests import Request
from starlette.status import HTTP_401_UNAUTHORIZED
# from config.database import Session as DBSession
from config.database import Session
from security.models import Token
from security.authentication import authenticate_user, create_access_token
from models.user import UserSecurity
from utils.database import get_db

app = FastAPI()


@app.middleware("http")
async def inject_db(request: Request, call_next):
    try:
        request.state.db = Session()
        response = await call_next(request)
    finally:
        request.state.db.close()
    return response


app.include_router(
    product.router,
    prefix="/products",
)

app.include_router(
    client.router,
    prefix="/clients"
)

app.include_router(
    user.router,
    prefix="/users"
)

app.include_router(
    stock.router,
    prefix="/stocks"
)


@app.get("/")
async def read_root():
    return "Banquev system"


@app.post("/token", response_model=Token)
async def login(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    authenticated_user: UserSecurity = authenticate_user(db, form_data.username, form_data.password)
    if not authenticated_user:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    return create_access_token(data={"sub": authenticated_user.email})
