import pytest
from sqlalchemy.orm import sessionmaker
from config.database import test_engine


Session = sessionmaker()


@pytest.fixture(scope='module')
def connection():
    connection = test_engine.connect()
    yield connection
    connection.close()


@pytest.fixture(scope='function')
def session(connection):
    transaction = connection.begin()
    session = Session(bind=connection)
    yield session
    session.close()
    transaction.rollback()