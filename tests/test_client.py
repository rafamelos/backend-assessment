from models.client import ClientCreate, Client
from typing import List
from datetime import datetime
from crud import client as crud


def test_find_all(session):
    for i in range(4):
        client: ClientCreate = ClientCreate(name="test" + str(i), phone="35898989" + str(i),
                                            created_at=datetime.utcnow())
        crud.save(session, client)

    result: List[Client] = crud.find_all(session)

    assert len(result) == 4


def test_find_by_id(session):
    client: ClientCreate = ClientCreate(name="test", phone="35898989", created_at=datetime.utcnow())
    saved_client = crud.save(session, client)

    result = crud.find_by_id(session, saved_client.id)
    assert result.id == saved_client.id
    assert result.name == saved_client.name
    assert result.phone == saved_client.phone
    assert result.created_at == saved_client.created_at


def test_save_client(session):
    client: ClientCreate = ClientCreate(name="test", phone="35898989", created_at=datetime.utcnow())
    saved_client: Client = crud.save(session, client)

    assert saved_client.name == client.name
    assert saved_client.phone == client.phone
    assert saved_client.created_at == client.created_at


def test_update_client(session):
    client: ClientCreate = ClientCreate(name="test", phone="35898989", created_at=datetime.utcnow())
    saved_client: Client = crud.save(session, client)

    saved_client.name = "test update"

    updated_client: Client = crud.update(session, saved_client)

    assert updated_client.id == saved_client.id
    assert updated_client.name == "test update"
    assert updated_client.phone == saved_client.phone
    assert updated_client.created_at == saved_client.created_at


def test_delete_client(session):
    client: ClientCreate = ClientCreate(name="test", phone="35898989", created_at=datetime.utcnow())
    saved_client: Client = crud.save(session, client)

    deleted_client: Client = crud.delete(session, saved_client.id)
    result: Client = crud.find_by_id(session, saved_client.id)

    assert result is None
    assert deleted_client.id == saved_client.id
    assert deleted_client.name == saved_client.name
    assert deleted_client.phone == saved_client.phone
    assert deleted_client.created_at == saved_client.created_at
