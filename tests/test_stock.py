from typing import List
from datetime import datetime
from crud import stock as crud
from crud import client as client_crud
from models.product import ProductForStock
from models.stock_product import StockProductCreate
from models.stock import StockCreate, Stock
from models.client import ClientCreate


def test_save(session):
    client: ClientCreate = ClientCreate(name="client1", phone="35987876565")
    saved_client = client_crud.save(session, client)

    products: List[StockProductCreate] = []
    product1: ProductForStock = ProductForStock(name="product1", type="type1", price=3, created_at=datetime.utcnow())
    stock_product1: StockProductCreate = StockProductCreate(quantity=4, product=product1)
    products.append(stock_product1)
    product2: ProductForStock = ProductForStock(name="product2", type="type2", price=4, created_at=datetime.utcnow())
    stock_product2: StockProductCreate = StockProductCreate(quantity=10, product=product2)
    products.append(stock_product2)

    stock: StockCreate = StockCreate(date=datetime.utcnow(), client_id=saved_client.id, products=products)

    saved_stock: Stock = crud.save(session, stock)

    assert saved_stock.date == stock.date
    assert saved_stock.client.id == saved_client.id
    assert len(saved_stock.products) == 2
