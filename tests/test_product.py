from models.product import ProductCreate, Product
from typing import List
from crud import product as crud


def test_find_all(session):
    for i in range(4):
        product = ProductCreate(name="test" + str(i), type="test1" + str(i), price=i)
        crud.save(session, product)

    result: List[Product] = crud.find_all(session)

    assert len(result) == 4


def test_find_by_id(session):
    product = ProductCreate(name="test", type="test", price=3.0)
    saved_product = crud.save(session, product)

    result = crud.find_by_id(session, saved_product.id)
    assert result.id == saved_product.id
    assert result.name == saved_product.name
    assert result.type == saved_product.type
    assert result.price == saved_product.price


def test_save_product(session):
    product: ProductCreate = ProductCreate(name="test1", type="test1", price=3.0)
    saved_product: Product = crud.save(session, product)

    assert saved_product.name == product.name
    assert saved_product.type == product.type
    assert saved_product.price == product.price


def test_update_product(session):
    product: ProductCreate = ProductCreate(name="test1", type="test1", price=3.0)
    saved_product: Product = crud.save(session, product)

    saved_product.name = "test update"

    updated_product: Product = crud.update(session, saved_product)

    assert updated_product.id == saved_product.id
    assert updated_product.name == "test update"
    assert updated_product.type == saved_product.type
    assert updated_product.price == saved_product.price


def test_delete_product(session):
    product: ProductCreate = ProductCreate(name="test1", type="test1", price=3.0)
    saved_product: Product = crud.save(session, product)

    deleted_product: Product = crud.delete(session, saved_product.id)
    result: Product = crud.find_by_id(session, saved_product.id)

    assert result is None
    assert deleted_product.id == saved_product.id
    assert deleted_product.name == saved_product.name
    assert deleted_product.type == saved_product.type
    assert deleted_product.price == saved_product.price
