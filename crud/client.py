from typing import List, Optional
from models.client import Client, ClientCreate, ClientOrm
from sqlalchemy.orm import Session


def find_all(db: Session) -> List[Client]:
    clients: List[Client] = []
    for client_orm in db.query(ClientOrm).order_by(ClientOrm.created_at).all():
        clients.append(Client.from_orm(client_orm))

    return clients


def find_by_id(db: Session, id: int) -> Optional[Client]:
    client_orm: ClientOrm = db.query(ClientOrm).filter(ClientOrm.id == id).first()
    if client_orm is None:
        return None
    return Client.from_orm(client_orm)


def save(db: Session, client: ClientCreate) -> Client:
    client_orm: ClientOrm = ClientOrm(name=client.name, phone=client.phone, created_at=client.created_at)
    db.add(client_orm)
    db.commit()
    db.refresh(client_orm)
    return Client.from_orm(client_orm)


def update(db: Session, client: Client) -> Client:
    db.query(ClientOrm).filter(ClientOrm.id == client.id).update(client)
    db.commit()
    return find_by_id(db, client.id)


def delete(db: Session, id: int) -> Optional[Client]:
    client = db.query(ClientOrm).get(id)
    if client is None:
        return None
    db.delete(client)
    db.commit()
    return Client.from_orm(client)
