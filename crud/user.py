from models.user import User, UserCreate, UserSecurity, UserOrm
from sqlalchemy.orm import Session
from security.crypt import create_password_hash


def find_user_security_by_email(db: Session, email: str) -> UserSecurity:
    user_orm: UserOrm = db.query(UserOrm).filter(UserOrm.email == email).first()
    if not user_orm:
        return None
    return UserSecurity.from_orm(user_orm)


def find_by_email(db: Session, email: str) -> User:
    user_orm: UserOrm = db.query(UserOrm).filter(UserOrm.email == email).first()
    if not user_orm:
        return None
    return User.from_orm(user_orm)


def save(db: Session, user: UserCreate) -> User:
    user_orm: UserOrm = UserOrm(name=user.name, email=user.email, password=create_password_hash(user.password), group_id=user.group_id)
    db.add(user_orm)
    db.commit()
    db.refresh(user_orm)
    return User.from_orm(user_orm)
