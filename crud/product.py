from typing import List, Optional
from models.product import Product, ProductCreate, ProductOrm
from sqlalchemy.orm import Session


def find_all(db: Session) -> List[Product]:
    products: List[Product] = []
    for product_orm in db.query(ProductOrm).order_by(ProductOrm.created_at).all():
        products.append(Product.from_orm(product_orm))

    return products


def find_by_id(db: Session, id: int) -> Optional[Product]:
    product_orm: ProductOrm = db.query(ProductOrm).filter(ProductOrm.id == id).first()
    if product_orm is None:
        return None
    return Product.from_orm(product_orm)


def save(db: Session, product: ProductCreate) -> Product:
    product_orm: ProductOrm = ProductOrm(name=product.name, type=product.type, price=product.price, created_at=product.created_at)
    db.add(product_orm)
    db.commit()
    db.refresh(product_orm)
    return Product.from_orm(product_orm)


def update(db: Session, product: Product) -> Product:
    db.query(ProductOrm).filter(ProductOrm.id == product.id).update(product)
    db.commit()
    return find_by_id(db, product.id)


def delete(db: Session, id: int) -> Optional[Product]:
    product = db.query(ProductOrm).get(id)
    if product is None:
        return None
    db.delete(product)
    db.commit()
    return Product.from_orm(product)
