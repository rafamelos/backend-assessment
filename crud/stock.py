from sqlalchemy.orm import Session
from models.stock import StockCreate, Stock, StockOrm
from models.stock_product import StockProductOrm
from models.product import ProductOrm


def save(db: Session, stock: StockCreate) -> Stock:
    stock_orm: StockOrm = StockOrm(date=stock.date, client_id=stock.client_id)
    products = []
    for stock_product in stock.products:
        if stock_product.product_id is None:
            product = stock_product.product
            product_orm: ProductOrm = ProductOrm(name=product.name, type=product.type, price=product.price,
                                                 created_at=product.created_at)
            stock_product_orm: StockProductOrm = StockProductOrm(quantity=stock_product.quantity, product=product_orm)
            products.append(stock_product_orm)
        else:
            stock_product_orm: StockProductOrm = StockProductOrm(quantity=stock_product.quantity,
                                                                 product_id=stock_product.product_id)
            products.append(stock_product_orm)
    stock_orm.products = products
    db.add(stock_orm)
    db.commit()
    db.refresh(stock_orm)
    return Stock.from_orm(stock_orm)
